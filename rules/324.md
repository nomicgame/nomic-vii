## 324/0

Every day at a random hour each player is assigned a different fruit from this list.  
https://ospi.k12.wa.us/sites/default/files/2023-08/fruita-z.pdf  
Each player has 10 guesses to guess their fruit and if they do they receive 1 of their fruit.  
Nomitron can give a hint related to their fruit.  
Hints are in order of request. Color, First letter of name, Type (eg. berry, stone, etc.)  
If a player fails guess their fruit after 10 guesses then that player receives a banana on the following day and loses it at the end of that day if unused.  
A player with a banana may throw that banana at any other player.  
A 1d8 is rolled to determine if they hit. Hits are on prime numbers only.
If a player is hit by a banana they have the rotten roll for the duration of that day.  
If a player has the rotten roll they cannot act to guess their fruit  
If a player has the rotten roll they acquire 1 Rot.  
If a player has at least 15 rot they are in Rot Girl Winter.
