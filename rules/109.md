## 109/0 &nbsp;~IMMUTABLE

A player is a person formally taking part in the game. The server moderators select the initial set of players upon the game’s commencement. Rules that facilitate other persons joining the game as new players are permitted.

Persons may not act as multiple players at once, nor may a player be composed of multiple persons. Persons in contravention of this rule may be banned from the game at the moderators’ discretion.

Players must be present on the server. If a person who is a player leaves or is removed from the server for more than 168 consecutive hours, said person is considered to have left the game and will no longer be a player.

A player always has the option to leave the game rather than continue to play or incur a game penalty.

A former player rejoining the game is treated as a new player. Player information is erased at the time of said player leaving the game and not carried over from the person’s previous time as a player.
