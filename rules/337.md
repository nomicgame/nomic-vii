## 337/0

### The Nomic Parliament

The Nomic Parliament is a 500-member body composed of delegates belonging to players' parties. Parties may have multiple delegates in the Nomic Parliament.

A term in the Nomic Parliament begins at the start of a turn, and ends at the end of that turn. At the start of each term, an election is automatically held to determine the parliament's party composition for the remainder of the turn.

### Elections

During the term before an election, players can stand their parties for the next election in #actions. All parties that are standing for election will divide seats in the election proportionally based on their electoral strength.

Parties have a base electoral strength. The base electoral strength for all parties is 1. Other rules may affect a party's base electoral strength.

At the time of an election, each party running for election is given a polling error multiplier, which is an integer percentage that can range from -30% to +30%, inclusive. The value of the polling error multiplier is determined uniformly at random.

If no parties have stood for election at the time of the election, the election is not held and the parliament is vacant for the term.

### Campaign Strategies

Parties that are currently standing for an election may choose a campaign strategy. Players choose a campaign strategy publicly, but the details of that choice, including which strategy the player chose, is private until the election, at which point it is revealed. Players cannot re-choose their campaign strategy for an election.

There are three campaign strategies a player can choose from are selfless, selfish, and nutritious.

A selfless campaign strategy results in a player gaining 5 fame at the election. A selfless campaign strategy is strong against a nutritious campaign strategy and weak against a selfish campaign strategy. A selfless campaign strategy's base campaign strategy multiplier is x1.2.

A selfish campaign strategy results in a player losing 5 fame at the election. A selfish campaign strategy is strong against a selfless campaign strategy and weak against a nutritious campaign strategy. A selfish campaign strategy's base campaign strategy multiplier is x2.0.

A nutritious campaign strategy results in a player losing 2 of their fruits at the election. Players have 24 hours from the election to choose which of their fruits to lose; fruits are lost at random afterwards if a player has not chosen. If a player has fewer than 2 fruits at the time of the election, they do not lose any fruit. A nutritious campaign strategy is strong against a selfish campaign strategy and weak against a selfless campaign strategy. A nutritious campaign strategy's base campaign strategy multiplier is x2.0 if the player using it has at least 2 fruits at the time of the election, and x0.5 if the player does not.

At the time of an election, each party running for election is given a campaign strategy multiplier based on their and their opponents' campaign strategies. Players that did not choose a campaign strategy have a campaign strategy multiplier of x1. Players that did choose a campaign strategy have a campaign strategy multiplier of their strategy's base campaign strategy multiplier, plus 0.2 for each opponent’s campaign strategy they are strong against, minus 0.2 for each opponent’s campaign strategy they are weak against. Campaign strategy multipliers have a minimum value of x0.05.

### Election Calculations

The retention percentage is the percentage of seats that are "carried over" from the previous term to the current term in an election. Seats that are "carried over" in this manner are referred to as "retained seats" and are equal in number to the number of total seats in the parliament times the retention percentage (if there is a party in the previous term’s parliament that is standing for election) or 0 (if there is not).

The retention percentage is 10%.

A party’s electoral strength in an election is their base electoral strength, times their campaign strategy multiplier, times their polling error multiplier. Other rules may add additional multipliers to this calculation

When an election is resolved, retained seats are first assigned, then all other seats are assigned.

If there are retained seats for an election, then retained seat assignment is done as follows:
* For each party that is both represented in the pre-election parliament and is standing for election, take the number of seats that the party has in the pre-election parliament as the party’s "population"
* Apply the Huntington-Hill apportionment method to fill the retained seats, using each party’s "population"

Non-retained seat assignment is done as follows:
* For each party standing for election, take their electoral strength for that election as the party’s "population"
* Apply the Huntington-Hill apportionment method to fill the non-retained seats, using each party’s "population"
