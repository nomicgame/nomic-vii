# NOMIC VII RULESET (RAW FORMAT)
---
## 101 &nbsp;~IMMUTABLE

All players must always abide by all the rules then in effect, in the form that they are then in effect. The game begins with the rules from the Initial Set in effect. The Initial Set consists of Rules 101-116 (which are immutable) and 201-212 (which are mutable).

## 102 &nbsp;~IMMUTABLE

Rules have properties. Other rules may add additional properties for rules along with said properties’ default values, so long as said properties do not contradict the properties defined in this rule.

A rule’s number is a natural number used for reference. The set of rules currently in effect shall never contain multiple rules with the same number. A rule’s number cannot be modified.

A rule’s mutability designates whether said rule is mutable or immutable. Mutability must be either mutable or immutable; it cannot be neither nor both. Initially, rules in the 100s will be designated immutable and rules in the 200s mutable. Transmutation is defined as the modification of a rule’s mutability. Rules are mutable by default.

A rule’s body is the main section of the rule. For any rule currently in effect, players must always abide by the contents of this property.

## 103 &nbsp;~IMMUTABLE

A rule-change is either of the following:

* The enactment or repeal of a mutable rule
* The modification of one of a rule’s properties

## 104 &nbsp;~IMMUTABLE

A proposed rule-change, or proposal, is a motion to adopt a rule-change. All proposals shall be adopted if and only if they are put to a rules-defined vote and they receive the required number of votes.

Proposals have properties. Other rules may add additional properties for proposals along with said properties’ default values, so long as said properties do not contradict the properties defined in this rule.

A proposal’s rule-change is the rule-change that will take effect if the proposal is passed. Proposals must have a rule-change, and a proposal’s rule-change may be changed if explicitly allowed and if the proposal is not currently being put to vote.

A proposal’s number is a natural number used for reference once the proposal is put to a vote. Proposals not yet put to vote do not have numbers, and a proposal’s number cannot be changed once it is assigned.

A proposal’s author is the player who created the proposal. If the proposal was not created by a player, then the proposal does not have an author. If the player who created the proposal leaves the game, then the proposal loses its author. A proposal’s author cannot be modified otherwise.

## 105 &nbsp;~IMMUTABLE

An adopted rule-change takes full effect at the moment of the start of the turn following the turn that it was voted on in the proper way. No rule-change may have retroactive application or change prior game state.

## 106 &nbsp;~IMMUTABLE

The first proposal put to vote receives a number of 301. Each proposal put to a vote in the proper way receives the next successive number, whether or not the proposal is passed. Additional rules may clarify how numbers are distributed among proposals that are put to vote simultaneously.

When a proposal containing a rule-change that enacts a new rule is passed, the new rule receives said proposal’s number.

## 107 &nbsp;~IMMUTABLE

Rule-changes that transmute immutable rules into mutable rules may be adopted if and only if the vote is unanimous among the voters. Transmutation shall not be implied, but must be stated explicitly in a proposal to take effect.

## 108 &nbsp;~IMMUTABLE

In a conflict between a mutable and an immutable rule, the immutable rule takes precedence and the conflicting section of the mutable rule is void.

If two or more mutable rules conflict with one another, or if two or more immutable rules conflict with one another, then the rule with the lowest number takes precedence. If at least one of the rules in conflict explicitly says of itself that it defers to another rule (or set of rules) or takes precedence over another rule (or set of rules), then such provisions shall supersede the numerical method for determining precedence. If two or more rules claim to take precedence over one another or to defer to one another, then the numerical method again governs.

## 109 &nbsp;~IMMUTABLE

A player is a person formally taking part in the game. The server moderators select the initial set of players upon the game’s commencement. Rules that facilitate other persons joining the game as new players are permitted.

Persons may not act as multiple players at once, nor may a player be composed of multiple persons. Persons in contravention of this rule may be banned from the game at the moderators’ discretion.

Players must be present on the server. If a person who is a player leaves or is removed from the server for more than 168 consecutive hours, said person is considered to have left the game and will no longer be a player.

A player always has the option to leave the game rather than continue to play or incur a game penalty.

A former player rejoining the game is treated as a new player. Player information is erased at the time of said player leaving the game and not carried over from the person’s previous time as a player.

## 110 &nbsp;~IMMUTABLE

The game is ossified if both of the following conditions are true:

* It is impossible to change the rules of the game
* The game has either not ended or is not set to end at any specific time

If, but for this rule, the net effect of a rule would cause the game to be ossified, then the offending section or sections of said rule are void. If, but for this rule, the state of the game were to change in such a way as to render the game ossified, then said change is canceled and does not occur.

## 111 &nbsp;~IMMUTABLE

Rules can make the following changes to the ruleset:

* Change their own body
* Repeal themselves
* Define additional rule properties along with their default values

Rules may not make any other changes to the ruleset, nor change the interpretations of other rules, unless such a change is via the adoption of a rule-change. Rules that clarify or that define mechanisms that clarify, rather than change, the interpretations of other rules are permitted.

## 112 &nbsp;~IMMUTABLE

A player’s vote must be unambiguous. A vote that would be ambiguous is not considered to be a vote. Additional rules may be made to clarify what constitutes ambiguity.

## 113 &nbsp;~IMMUTABLE

Server moderators regulate the server and the game, regulate the components of the server and the game, and determine the state of the game.

Server moderators can, by majority decision:

* Ban and unban persons from joining the server
* Ban and unban persons from playing the game
* Add or remove persons as server moderators 

Server moderators are empowered to enact and enforce bans prior to the start of the game.

The game adheres to the Discord Terms of Service and the Discord Community Guidelines. Server moderators are empowered to remove content that violates the terms and guidelines as well as sanction violating persons, up to and including banning.

The instantiator of this instance of the game starts as a server moderator. If there are no server moderators, the instantiator becomes a server moderator.

## 114 &nbsp;~IMMUTABLE

All actions, including voting, endorsing, rule-change proposals, and public declarations, must occur in the locations designated for them. Any action that does not occur in said action’s designated location is not considered a valid action.

## 115 &nbsp;~IMMUTABLE

References to specific persons in a rule, including but not limited to references via legal name, username, display name, or commonly-used nickname, are considered to be references to the person or persons being referred to at the time the text containing said reference was proposed unless the rule explicitly states otherwise.

A person that is specifically referred to by a rule cannot stop being referred to by said rule by no longer associating with the identification being used in said rule, nor would a person start being referred to by a rule if they were to change their identification to match the rule's reference.

References to a group of persons based on their identification fall outside the scope of this rule.

## 116 &nbsp;~IMMUTABLE

Whatever is not prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.

## 201

The Proposal Queue is a queue that proposals go in. Players may have up to one proposal in the Proposal Queue. Active players may create a proposal to go in the Proposal Queue at any time; if a player that already has a proposal in the Proposal Queue creates another one, the first proposal is removed. Players may also remove their proposals from the Proposal Queue without replacement. Proposals in the Proposal Queue may not have their rule-changes edited.

The Deck is a collection that proposals go on. A proposal that is on the Deck can have its rule-change edited by that proposal’s author.

Active players may freely endorse and unendorse proposals in the Proposal Queue. Proposals in the Proposal Queue are sorted first in descending order of number of active endorsing players, then in ascending order of proposal creation time.

Players endorse their own proposals in the Proposal Queue by default.

## 202

At the start of day on Tuesday, Thursday, and Saturday (UTC−05:00), the top proposal in the Proposal Queue is removed from the Proposal Queue and added onto the Deck.

At the start of day on Wednesday, Friday, and Sunday (UTC−05:00), all proposals on the Deck are removed from the Deck and put to a vote lasting for 24 hours.

The length of a turn defaults to 168 hours.

Players can cast a deferred vote for a proposal while said proposal is on the Deck and not yet up to vote. A player's most recent deferred vote for a given proposal will be considered to be a player's vote for said proposal at the start of the proposal's voting period if there have been no deck edits for said proposal since said vote was cast.

If a player that already has a proposal in the Proposal Queue submits another proposal, then the first proposal is replaced with the new proposal and keeps existing endorsements, superseding Rule 201.

## 203

A proposal is passed if and only if it receives a simple majority of votes in favor of its passing when compared to those against its passing.

Other rules may introduce additional conditions necessary for passing a proposal, notwithstanding the above paragraph.

## 204

All players start the game as active. An active player becomes inactive (*i.e.* loses their active status) if said active player has publicly declared that they are now inactive.

A player who has become inactive may attempt to become active again by declaring that they are active, attempting to cast a vote, or attempting to endorse a proposal. If the player then becomes active, then the vote or endorsement will be counted as though the player had cast or granted it while active.

Other rules can affect a player’s activity.

## 205

Players required by the rules to perform an action have a total of 48 hours to perform said action unless otherwise specified. A player who does not perform said action within the time allotted to them becomes inactive and cannot become active until either they do perform said action or said action becomes impossible to perform.

## 206

Each active player can vote up to once per vote. Inactive players cannot vote.  
Players may change their votes by voting again. Players may withdraw their votes with the phrase “withdraw” or “abstain” or "strawberry". Players may not edit nor delete messages that contain votes. An edited or deleted message’s vote is invalid.

## 207

This rule supersedes Rule 206.

The following text phrases, and only the following phrases, are considered to be unambiguous affirmative votes:

* aye
* yay
* yes
* y
* ye
* ya
* yea
* yeah
* yeag
* heck yeah
* hell yeah
* sí
* okay
* okey dokey
* i concur
* iconic
* apples
* apricot
* banana
* blackberry
* boysenberry
* cherry
* clementine
* cranberry
* crenshaw melon
* currants
* dates (tree dried only)
* grapes
* grapefruit
* guava
* horned melon
* jujube
* kiwi
* kumquat
* lemon
* lime
* loganberry
* lychee
* mandarin
* mamoncillo
* minneola
* nectarine
* blood orange
* papaya
* passion fruit
* peach
* pineapple
* plum
* rambutan
* raspberries
* satsuma
* soursop
* tangelo
* tangerine
* watermelon

The following text phrases, and only the following phrases, are considered to be unambiguous negative votes:

* nay
* no
* n
* nah
* nein
* cringe
* nuh uh
* no way jose
* nix
* ew
* boo 🍅
* fart
* blueberry
* canary melon
* cantaloupe
* casaba melon
* cherimoya
* christmas melon
* dragon fruit
* durian
* figs
* gooseberry
* honeydew melon
* jack fruit
* longan
* loquat
* mango
* mangosteen
* musk melon
* nance
* orange
* pear
* persimmon
* pomegranate
* prickly (cactus) pear
* pommelo
* pulasan
* star fruit (carambola)
* tamarillo
* ugli fruit

Phrases for voting are case-insensitive.

## 208

If the server moderators deem that any section of a rule is infeasible to implement manually to the point that a bot is required, and there is no such bot functional, then said rule’s section is void until said bot is functional.

## 209

If any player disagrees on the legality of a move or the interpretation or application of a rule, any of those players may insist on invoking Judgment. During Judgment, a randomly selected eligible player is to be the Judge and decide the question.

A term for Judges starts when Judgment is invoked without a current term, and ends at the start of the first turn that starts more than 168 hours after the start of the term. Invoking Judgment during the middle of a term does not affect the term’s length or the incumbent Judge.

The Judge is selected at the start of a term, or in the middle of an ongoing term if the previous Judge is disbarred. A player is eligible to be selected as Judge if and only if said player fulfills all of the following criteria:

* The player is active
* The player has voted on a proposed rule-change during the previous turn, or the player has voted on a proposed rule-change during the current turn, or fewer than two turns have ended
* The player has not already been Judge for the current term
* The player has opted in to being selected to be Judge

The incumbent Judge settles all questions arising from the game until the end of their term, including questions as to their own legitimacy and jurisdiction as Judge.

A Judge may be disbarred for the remainder of their term, in which case all Judgments made by said Judge during their current term are considered invalid and another random eligible player is selected to serve as Judge for the remainder of the term. The following events invoke disbarment:

* A three-fourths majority of all active players vote in favor of the Judge’s disbarment
* A simple majority of all server moderators vote in favor of the Judge’s disbarment
* The Judge does not publicly acknowledge their position as Judge within 24 hours of Judgment’s invocation
* The Judge recuses themselves from their position as Judge

Server moderators can only disbar a Judge in their capacity as server moderators if their Judgment egregiously conflicts with the rules.

A Judge may also resign from their position as Judge, which ends the term early. Unlike recusal, resignation does not invoke disbarment and the former Judge’s Judgements are not considered to be invalid.

Any players that, in #actions, have indicated a willingness to opt in to being selected as Judge between the start of the game and the moment this clause first comes into effect are considered to be opted in to being selected as Judge at the moment this clause first comes into effect.

Judges are not bound by the decisions of previous Judges. Judges may, however, settle only those questions on which Judgment has been invoked and that affect the current state of the game. All decisions by Judges shall be in accordance with all the rules then in effect, but when the rules are silent, inconsistent, or unclear on the point at issue, then the Judge shall consider game custom and the spirit of the game before applying other standards.

## 210

A Judicial Proposal is a kind of proposal that Judges may create to address or complement a Judgment they have made during their current term.

Incumbent Judges may create only one Judicial Proposal per term. Judicial Proposals are put onto the Deck and the start of the turn following their creation if and only if the Judge was not disbarred for the remainder of their term.

If a Judicial Proposal is put to vote simultaneously with proposals originating from the Proposal Queue, then it receives the first available number after the other proposals being put to vote have received theirs.

Judicial Proposals may have their rule-changes edited and may be deleted by their respective authors before they go on the Deck. An author that deletes a Judicial Proposal may create a replacement Judicial Proposal if and only if they are the incumbent Judge.

## 211

Unless otherwise specified, when a player declares one or more winners and a moderator has seconded this claim, then the game enters an End State. A moderator cannot second their own win.

The purpose of an End State is to adjudicate and come to a consensus on the existence and identity of the game’s winner or winners. During an End State, all in-game mechanics except for those outlined in the Initial Set are suspended, and Judgment on the claim of victory is considered to have been automatically invoked. The suspension of in-game mechanics does not affect or negate the claim of victory.

The current Judge can declare an End State to have been erroneously declared, in which case the End State ends prematurely and all suspended mechanics resume.

If, during an End State, a turn would end while the game has been in said End State for fewer than 168 hours, then the turn is extended by 168 hours instead.

If, during an End State, a turn would end while the game has been in said End State for at least 168 hours, then the game ends instead.

## 212

Persons wishing to become players may request to do so publicly. If no player objects to the requester joining the game within 24 hours of the request, either publicly or privately (to a server moderator), the requester will join the game as a new player at the start of the next turn. If such an objection occurs, a vote is immediately held on whether or not the requester joins the game. If there is a majority vote in favor 24 hours following the start of the vote, the requester will join the game as a player.

## 302

Proposal: Commotion In The Otion

If a player has 24 hours or more to perform an action with a set deadline, they may file a Motion For Continuance. Said Motion:

1\) Cannot be filed if:  
&nbsp;&nbsp;&nbsp;&nbsp;a) There are less than 24 hours before the deadline for a player to perform an action, or;  
&nbsp;&nbsp;&nbsp;&nbsp;b) The action does not have a set deadline  
2\) Cannot be filed by a player on behalf of anyone but themselves.  
3\) Shall not have a specific mandated format, but must unambiguously include the following things:  
&nbsp;&nbsp;&nbsp;&nbsp;a) A textual statement from the filing player in which they expressly and publicly communicate that they believe they will be unable to meet an action’s deadline.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) A player may provide a reason as to why they believe they will be unable to meet the deadline if they so choose, but no player shall be legally compelled to provide a reason.  
&nbsp;&nbsp;&nbsp;&nbsp;b) An explicit request for a new deadline of the player’s choosing.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) The new deadline must not be implicit or vague. A specific additional amount of time must be requested, i.e. something like 5 more days or 30 more hours.

A player may submit multiple Motions for the same action.

A Motion For Continuance must be approved by a Judge. If there is no Judge when a Motion is filed, one shall be appointed in the normal manner as described in rule 209. (If there is no rule 209 or method of appointing a Judge, then it is simply impossible for a Motion For Continuance to be approved). A simple majority of all server moderators or a three-fourths majority of all active players may overrule an approval if the new deadline has not arrived yet, but if the action is performed or the deadline has passed, there shall be no method of retroactively deciding that the deadline was invalid unless the deadline conflicted with the rules.

Approval can be denied or approved for any or no reason. No circumstances, however dire or mundane, shall be enshrined as always justifying an approval or lack thereof.

## 306

All players have an integer amount of fame. Anytime a rule attempts to set a player’s fame it is truncated to the integer. A negative fame represents infamy rather than a lack of notoriety.

* Players with less than or equal -100 to fame are nefarious
* Players with less than or equal to -25 fame are infamous
* Players with more than or equal to 25 fame are famous
* Players with more than or equal 100 fame are influential.

Fame cannot be the sole qualification of a win condition.

All new players start with zero fame. Other rules may add or subtract fame. Fame can also be tied to a specific status such that whenever you gain that status you gain fame, and lose fame when you lose that status.

Having a new rule you proposed enacted as a rule gives you +4 fame per rule. Amendments and repeals do not grant this bonus.

## 307

**The Funniest Proposal In Nomic**

There are four different types of humors: sanguine, choleric, melancholic, and phlegmatic.

Players can have a non-negative integer quantity of each type of humor. Players start out with 0 of each type of humor.

A player is Imbalanced if, for any two types of humor, the difference in quantities between said types that said player possesses exceeds 2. A player that is not Imbalanced is Balanced.

## 308

Office Hours

Players are encouraged to gather for proposal workshops in the Nomic voice chat during the following designated times:  
Tuesdays 7pm-10pm CDT  
Saturday 2pm-5pm CDT  
Sunday 11am-1pm CDT

## 310

The Nom Times Trademark

No players other than Bobo Kobo may publish under or use the name or variants of the name “The Nom Times” without public consent by Bobo Kobo.

## 312

Proposal: Paragon Of The People

A whole week of a month is a game week in which every day of that week falls within the same calendar month.

During the second-to-last whole week of every month, players may announce that they are running for the position of Paragon Of The People ("Paragon"). Players may announce or rescind their candidacy freely and without limit during this period, but only their final decision is considered valid.

During the last whole week of every month, players may vote in an election for Paragon from among players who ultimately decided to remain candidates. Players may change their vote for Paragon any number of times. Only a player’s final vote in a given election is counted.

The player who wins a given election becomes Paragon until the end of the next election, at which point they are stripped of the title unless they are also the player who won the new election. If an election would result in a tie, the first candidate amongst those who tied to say ‘First!’ after the final vote for that election is tallied in any text channel on the Nomic VII server is elected.

Players cannot run for Paragon if they;

1\) are a moderator;
2\) are inactive;
3\) have won both of the previous two elections, or;
4\) have won four previous elections in the current calendar year.

The following powers are bestowed upon the Paragon:

1\) If the server moderators vote to disbar a judge, they must allow the Paragon to take part in their vote, superseding the sixth bullet-point in rule 209.
2\) If the server moderators vote to overrule approval of a Motion For Continuance, they must allow the Paragon to take part in their vote, superseding rule 302. 
3\) A Paragon may second a player's win, invoking an end state, superseding the first sentence of 211, though they may not second their own win.
4\) Players may petition the Paragon to post memes in channels in which memes would otherwise be disallowed. If the Paragon deems the meme to be funny, the petitioner may post that meme in the desired channel.
5\) The Paragon can create Discord events so long as they are scheduled within their current term in office.
6\) Any rules passed after the passing of this rule that describe server moderators voting are assumed to involve the participation of the Paragon.

## 320

**Spieltag: Pt. I**

The Alignment Chart is 201x101 grid. The horizontal axis of the grid is considered to be the institutionalist-accelerationist axis (on the left and right, respectively), and the vertical axis of the grid is considered to be the moderatorian-anti-moderatorian axis (on the top and bottom, respectively).

Once per game, a player can roll alignment by using the `!rollAlignment` command in actions. Their alignment is then set to be a point on the grid, with the point's coordinates randomly chosen from all integer coordinate pairs wholly on the quadrant or quadrants with the fewest number of player alignments. No alignment coordinate can be 0.

A player that has rolled alignment can create one party per game, specifying the party's name and color. A party's name and color can be changed within 24 hours of its creation, but not after.

## 321

Players who do not explicitly call your vote on at least one proposal during the turn lose 1 fame. If a vote is nullified by a deck edit, that vote still counts as having been explicitly called.

To explicitly call your vote, you must respond with one of the unambiguous voting phrases listed in rule #207. In addition, players may abstain from voting by abstaining with an explicit "abstain" message instead of voting in the affirmative or negative cases. Your vote must be cast in an voting channel during a voting window either on the day of voting or the day of deck editing.

Inactive players cannot lose fame from this rule.

## 322

A Humorless Rule

If a player's humors are Imbalanced then they only gain 50% of the fame that is rewarded from any given source  
This rule supersedes all fame granting rules

## 324

Nomic of the Clock:  
Every day at a random hour each player is assigned a different fruit from this list.  
https://ospi.k12.wa.us/sites/default/files/2023-08/fruita-z.pdf  
Each player has 10 guesses to guess their fruit and if they do they receive 1 of their fruit.  
Nomitron can give a hint related to their fruit.  
Hints are in order of request. Color, First letter of name, Type (eg. berry, stone, etc.)  
If a player fails guess their fruit after 10 guesses then that player receives a banana on the following day and loses it at the end of that day if unused.  
A player with a banana may throw that banana at any other player.  
A 1d8 is rolled to determine if they hit. Hits are on prime numbers only.
If a player is hit by a banana they have the rotten roll for the duration of that day.  
If a player has the rotten roll they cannot act to guess their fruit  
If a player has the rotten roll they acquire 1 Rot.  
If a player has at least 15 rot they are in Rot Girl Winter.

## 336

This song is the national ring tone of nomic https://www.youtube.com/watch?v=iI7SuUyUImQ

Any player may request the song to play with a slash command that is mod defined.  
This can only be done once per day by only 1 player.

## 337

## Spieltag Pt. II

### The Nomic Parliament

The Nomic Parliament is a 500-member body composed of delegates belonging to players' parties. Parties may have multiple delegates in the Nomic Parliament.

A term in the Nomic Parliament begins at the start of a turn, and ends at the end of that turn. At the start of each term, an election is automatically held to determine the parliament's party composition for the remainder of the turn.

### Elections

During the term before an election, players can stand their parties for the next election in #actions. All parties that are standing for election will divide seats in the election proportionally based on their electoral strength.

Parties have a base electoral strength. The base electoral strength for all parties is 1. Other rules may affect a party's base electoral strength.

At the time of an election, each party running for election is given a polling error multiplier, which is an integer percentage that can range from -30% to +30%, inclusive. The value of the polling error multiplier is determined uniformly at random.

If no parties have stood for election at the time of the election, the election is not held and the parliament is vacant for the term.

### Campaign Strategies

Parties that are currently standing for an election may choose a campaign strategy. Players choose a campaign strategy publicly, but the details of that choice, including which strategy the player chose, is private until the election, at which point it is revealed. Players cannot re-choose their campaign strategy for an election.

There are three campaign strategies a player can choose from are selfless, selfish, and nutritious.

A selfless campaign strategy results in a player gaining 5 fame at the election. A selfless campaign strategy is strong against a nutritious campaign strategy and weak against a selfish campaign strategy. A selfless campaign strategy's base campaign strategy multiplier is x1.2.

A selfish campaign strategy results in a player losing 5 fame at the election. A selfish campaign strategy is strong against a selfless campaign strategy and weak against a nutritious campaign strategy. A selfish campaign strategy's base campaign strategy multiplier is x2.0.

A nutritious campaign strategy results in a player losing 2 of their fruits at the election. Players have 24 hours from the election to choose which of their fruits to lose; fruits are lost at random afterwards if a player has not chosen. If a player has fewer than 2 fruits at the time of the election, they do not lose any fruit. A nutritious campaign strategy is strong against a selfish campaign strategy and weak against a selfless campaign strategy. A nutritious campaign strategy's base campaign strategy multiplier is x2.0 if the player using it has at least 2 fruits at the time of the election, and x0.5 if the player does not.

At the time of an election, each party running for election is given a campaign strategy multiplier based on their and their opponents' campaign strategies. Players that did not choose a campaign strategy have a campaign strategy multiplier of x1. Players that did choose a campaign strategy have a campaign strategy multiplier of their strategy's base campaign strategy multiplier, plus 0.2 for each opponent’s campaign strategy they are strong against, minus 0.2 for each opponent’s campaign strategy they are weak against. Campaign strategy multipliers have a minimum value of x0.05.

### Election Calculations

The retention percentage is the percentage of seats that are "carried over" from the previous term to the current term in an election. Seats that are "carried over" in this manner are referred to as "retained seats" and are equal in number to the number of total seats in the parliament times the retention percentage (if there is a party in the previous term’s parliament that is standing for election) or 0 (if there is not).

The retention percentage is 10%.

A party’s electoral strength in an election is their base electoral strength, times their campaign strategy multiplier, times their polling error multiplier. Other rules may add additional multipliers to this calculation

When an election is resolved, retained seats are first assigned, then all other seats are assigned.

If there are retained seats for an election, then retained seat assignment is done as follows:
* For each party that is both represented in the pre-election parliament and is standing for election, take the number of seats that the party has in the pre-election parliament as the party’s "population"
* Apply the Huntington-Hill apportionment method to fill the retained seats, using each party’s "population"

Non-retained seat assignment is done as follows:
* For each party standing for election, take their electoral strength for that election as the party’s "population"
* Apply the Huntington-Hill apportionment method to fill the non-retained seats, using each party’s "population"

## 338

Taco Tuesdays

Each Tuesday, players will engage in Taco Tuesday by declaring a number of tacos they are eating in the actions channel. Players may only declare once how many tacos they are eating on this day. Players who do not declare a number of tacos are considered to be eating 0 tacos.

1. When eating 0 Tacos, the player loses 1 choleric humor (unless they have 0 choleric humor, in which case no action is taken against them.)
2. When eating 1-10 Tacos, the player has a 37.5% chance of gaining 1 phlegmatic humor, a 37.5% chance of gaining 1 sanguine humor, a 20% chance no action will be taken against them, and a 5% chance that they will lose 1 choleric humor. Only one of these consequences/benefits will occur.
3. When eating 11-126 Tacos, the player has a 20% chance of gaining 1 melancholic humor, a 20% chance of gaining 1 sanguine humor, a 15% of losing 1 phlegmatic humor, a 15% chance of losing 1 sanguine humor, and a 30% that no action will be taken against them. Only one of these consequences/benefits will occur.
4. When eating 127 tacos, the player has a 10% chance of becoming inactive for the entirety of the next turn, a 45% chance that they will gain 1 of all four humors, and a 45% percent chance that they will lose 1 of all four humors. Only one of these consequences/benefits will occur. In addition to this, a player eating 127 tacos has a 50% chance of being crowned the Taco Glutton and gaining 1 choleric humor. Once a player is crowned Taco Glutton, no other players may be crowned in that turn.
5. When eating 128 tacos or more, the player has a 100% chance that they will become inactive for the entirety of the next turn.

Each time a player becomes Taco Glutton, numbers 3, 4, and 5 from the above list's maximum number of tacos is increased by one.

The Taco Glutton must make a speech via voice call during one of the office hour sessions in the same turn. If no players are present for their speech, the Taco Glutton loses 3 fame. If players are present for the speech, they will all declare once the speech is over whether they will increase or decrease the Taco Glutton's fame by one. If a Taco Glutton does not make a speech in the allotted time, they lose 20 fame. The Taco Glutton loses their title when the turn ends.

The Taco Glutton's speech must be at least two minutes in length and must include the following:  
I. A list of people whom they would like to thank for this honor  
II. The number of tacos they ate

If a player is made or is going to be made inactive by this rule, they can prevent this or become active again by eating a taco-like meal in real life and posting proof.  
A taco- like meal is a meal that contains at least three of the following things:

A. A tortilla shell  
B. A meat or meat substitute  
C. A dairy-based ingredient or dairy substitute   
D. A vegetable

## 340

The Overcomplicated Refresher

If a player is inactive for 3 full turns in a row they are considered Dead and given the Dead role.

Any player who is Dead shall be treated as inactive but they cannot become active or remove the Dead role until they complete the Revival Quest.

The Revival Quest is as follows: a Dead player must make an attempt to read (or reread) the beginners guide for nomic and ask a question in game about any rule passed during their death After that they are no considered Dead, the Dead role is removed and they become active unless rendered inactive from not completing an action set by any other rule.

This rule does not apply retroactively from the date of its passing.

## 341

**Intro**

Welcome to the strange new world of Fruit! There are 68 different fruits to collect and  
battle with! My name is Professor Frutcus and I am here to detail how you can  
participate in the world of Fruit battling!

**Championship**

The player who wins the most unique, individual fruit battles in a turn receives a random  
fruit badge. To qualify to be considered to be champion a player must have won at least  
3 battles in that turn. Fruit battles do not count if a player beats the same opponent  
player twice in the same turn. There are badges for each type of fruit (Berry, Capsule,  
Drupe, Etaerio, Pome, Syncarp). The player that receives them all is the champion. If a  
player has rolled a badge they already have they instead received a badge point. 10  
badge points may be redeemed to force the next roll of a badge to be a badge they do  
not already have. This is still a random roll from the pool of badges that players have  
not received yet. Players that have redeemed 10 badge points have the “Forced Roll”  
indicator on their player and keep that indicator until they win a badge. Badges are  
stored in Player Data.


**Battle**

Players may request a battle from a player with the command /battle (player name) in  
actions.  
The requested player must accept the battle for it to commence.  
Nomitron rolls attacks until all of one of the player's Fruits have lost all of their health  
points.  
The winning player of the battle gets Fame equal to the total level of all of the  
opponents fruits. The losing player loses the same amount of Fame won by the winning  
player.  
Fame is awarded and lost after a battle has been won.  
A battle is won when one of the players has lost all the health points in each of their  
fruits. That player who has no fruit with health points is the loser and the other the  
winner of the battle.  
When a fruit loses all its health points and there is another fruit in the party the next fruit  
in the party enters the fight automatically and continues to fight.  
When a battle is won nomitorn alerts the two players with a message in actions. This  
message lists the player who won the battle and the player who lost. The amount of  
fame the winning player won and the amount of fame the losing player lost.

**Party**

Players may select 3 Fruits to be added to their party.  
Players may only add fruits to their party from fruits they own in their player data. The  
fruits selected to be in the party are then moved to their own party.  
The order in which they are selected is the order they battle in when in battle.  
Players may swap out which Fruit is in their party anytime they are not in battle.  
Players parties are public.

**Attack**

All Fruits roll a 1d8 attack and defense rolls.  
If a Fruit is Super effective against its opponent they roll an addition 1d3 and add that to  
their attack and defense rolls.  
If a Fruit is weak to an opponent they reduce their roll by 2 for attack and defense rolls.  
Below is a chart that describes the fruits weaknesses (W) and who they are Super  
effective (S) against.  
The fruit that rolls higher than the opponent's fruit then does 1 damage to that fruit's  
health.  
If a fruit rolls an 8 and a 3 on a super effective attack roll and the opponent's fruit rolls a  
1 then the fruit that rolled a 1 receives a critical health and loses all of its health points.  
Attacks are automated by nomitron and continue until one of the fruits in a battle has  
been defeated.  
Defeat or being defeated is when a fruit has 0 health points remaining.  
When a fruit is defeated the next fruit in the player's party enters the battle and the  
battle continues with the winning fruit in the other player's party.

**Leveling up and Healing**

Players can heal their fruit using another fruit of the same name on that fruit with the  
command /heal (fruit).  
Healing consumes 1 of that type of fruit to fully heal the fruit in their party.  
Fruits cannot be over healed.  
A Fruits level is equal to the number of battles that fruit has won.  
If a player loses a battle they run to Nurse Jane to receive a full heal to their fruit.  
Fruit heal naturally over time. Fruit heal 2 health points per day.  
Players may pay 10₦ to Nurse Jane to heal their team any time not in battle.

<ins>*Fruit Health Data*</ins>

Berry-14  
Capsule-20  
Drupe-12  
Etaerio-13  
Pome-10  
Syncarp-16

<table>
  <tbody>
    <tr>
      <td></td>
      <td><b><i>Attack<br>down</i></b></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><b><i>Defense<br>across</i></b></td>
      <td></td>
      <td><b>Berry</b></td>
      <td><b>Capsule</b></td>
      <td><b>Drupe</b></td>
      <td><b>Etaerio</b></td>
      <td><b>Pome</b></td>
      <td><b>Syncarp</b></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Berry</b></td>
      <td></td>
      <td><b>S</b></td>
      <td><b>W</b></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Capsule</b></td>
      <td><b>W</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td><b>S</b></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Drupe</b></td>
      <td><b>S</b></td>
      <td></td>
      <td></td>
      <td><b>W</b></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Etaerio</b></td>
      <td></td>
      <td></td>
      <td><b>S</b></td>
      <td></td>
      <td></td>
      <td><b>W</b></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Pome</b></td>
      <td></td>
      <td><b>W</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td><b>S</b></td>
    </tr>
    <tr>
      <td></td>
      <td><b>Syncarp</b></td>
      <td></td>
      <td></td>
      <td></td>
      <td><b>S</b></td>
      <td><b>W</b></td>
      <td></td>
    </tr>
  </tbody>
</table>

## 344

At the start of each turn, all players who have not tried to vote once in the previous two turns become inactive.  
Players may also declare themselves as "On Leave".

Players who have been inactive for more than 7 days and/or are "On Leave" cannot be affected by game mechanics (other than those defined in the initial ruleset and office hours), but they also cannot interact with game mechanics (other than those defined in the initial ruleset and office hours).

Game mechanics are defined as nomic-specific actions as defined by the rules.

Players who are inactive because they are "On Leave" may declare that they are no longer on leave, which will return them to their natural active player state.  
Players who are inactive because of non-voting may become active again by attending and participating in office hours or by submitting a proposal in main-proposals.

## 345

Daniel's (\@Crorem) eternal soul is stored within nomic.  
As a side effect: Nomitron can secure proper citizenship now that Nomitron has a soul.

## 346

## Spieltag Pt. III

### The Indicator Puck

The Indicator Puck represents a moving point on the Alignment Chart, starting at the center of the Chart (*i.e.* (0, 0)).

If a coordinate were to increase or decrease so that the indicator would be outside the Alignment Chart, it is instead increased or decreased so that the Indicator Puck is on the edge of the Chart.

A player/party’s distance from the Indicator Puck is the euclidean distance between the Puck and the player/party’s alignment on the Alignment Chart.

### Motions

Each player/party in the Nomic Parliament may make up to 1 motion per term. Players in the parliament will subsequently vote on whether or not to pass said motion. A player’s vote is equivalent to that player’s party’s vote. There are no abstaining votes.

Between 24 hours after the start of a term and 24 hours before the end of a term, players in that term’s Nomic Parliament may make motions. Motions are voted on during the day (starting at UTC-05:00) following their creation. Motions pass at the end of that day if and only if a simple majority of delegates in the Nomic Parliament vote in their favor. Motion votes are resolved prior to elections if a motion vote resolution and election were to occur simultaneously.

There are two kinds of motions: Alignment Motions and Rule Motions. Players by default vote for their own motions, notwithstanding the clauses below.

Alignment Motions specify an alignment (one of institutionalism, accelerationism, moderatorianism, or anti-moderatorianism). If an Alignment Motion passes, then the Indicator Puck is moved by 6 + 1d7 in the direction of the specified alignment.

Players by default vote for an Alignment Motion if moving the Indicator Puck by 10 in the direction of the specified alignment would decrease their own distance from the Puck, and vote against it if it would not.

Rule Motions specify a rule-change proposal. If a Rule Motion passes, then the specified proposal is put onto the Deck the next time a proposal from the Proposal Queue is put onto the deck (or would be, if there are no proposals in the Proposal Queue). Proposals put to vote in this manner receive the first available number after proposals from the Proposal Queue and Judicial Proposals have received theirs, superseding Rule 210.

Players cannot make Rule Motions if at least two Rule Motions have been passed during the current term.

Players by default vote against a Rule Motion.

### Cohesion

Delegates in the Nomic Parliament do not vote automatically with their party. When a vote is resolved, each delegate votes with their party with chance *x*, and votes against their party with chance 1-*x*. This variable *x* is a party’s Cohesion for a given motion.

A party’s Base Cohesion is equal to 1 - 2*n*/5, where *n* is the proportion of all non-vacant seats in the parliament that the party has. In other words, Base Cohesion linearly decreases from 100% to 60% as a party goes from having no seat in the Nomic Parliament to having every seat.

A party’s Cohesion for a given motion is equal to their Base Cohesion, plus (1d21 - 10)/100. Cohesion cannot exceed 1.

### Coalitions

For the first 24 hours of a Nomic Parliament term, players/parties may form, join, and leave coalitions, which are formal groups of parties. Parties cannot be in more than one coalition at a time.

A player whose party is not in a coalition can form and automatically join one by specifying the coalition’s name and color. Parties may join a coalition with the unanimous consent of the joining player/party and every player/party already in the coalition.

Parties may leave a coalition unilaterally. Parties may also be kicked out of a coalition if there are at least two other parties in the coalition and those other parties unanimously consent to having the party in question kicked out.

Parties automatically leave a coalition at the start of a parliament’s term if they have no representation in parliament during that term. A coalition is automatically disbanded if at any point it has no parties.

Players cannot join, form, or leave coalitions at any other point in the parliament’s term.

If, when a vote on a motion is resolved, all parties in a coalition have voted the same way, then each party’s Base Coalitive Cohesion is used in place of their Base Cohesion. A party’s Base Coalitive Cohesion is equal to (*N* - 1 + *x*) / *N*, where *x* is the party’s Base Cohesion and *N* is the inverse of the sum of the squares of each party in the party’s coalition’s proportion of seats in said coalition.

### Electoral Strength

Two values are added to a party’s base electoral strength as an election is resolved: a party’s Revolutionary Fervor and a party’s Mandate of Heaven.

A party’s Revolutionary Fervor is equal to the square root of the party’s distance from the Indicator Puck.

A party’s Mandate of Heaven is by default equal to 0 and can change in the following ways:
* If a party’s Mandate of Heaven is equal to 0, was equal to 0 from immediately prior to the most recent election throughout the present moment, and that party is either part of a coalition controlling a majority of seats or controls a majority of seats by itself, the party’s Mandate of Heaven becomes 6
* A party’s Mandate of Heaven is decreased by 3 to a minimum of 0 after an election
* Every party’s Mandate of Heaven is decreased by 1 to a minimum of 0 after an Alignment Motion’s vote if not every party with a positive Mandate of Heaven voted the same way for said vote
* A party’s Mandate of Heaven is set to 0 if another party controls a majority of seats
* A party’s Mandate of Heaven is set to 0 if a coalition that they are not a part of controls a majority of seats

A party's electoral strength in an election is additionally multiplied by their incumbency fatigue multiplier. A party's incumbency fatigue multiplier is x1.00 by default. For every full day that the indicator puck is fully within a quadrant, the following changes happen to parties' incumbency fatigue multipliers:

* x0.02 is subtracted from the incumbency fatigue multipliers of all parties whose alignments are in said quadrant, to a minimum of x0.33
* x0.04 is added to the incumbency fatigue multipliers of all parties whose alignments are not in said quadrant, to a maximum of x1.00

These calculations are assessed after voting on motions for the given day is resolved.

### Political Capital

Political Capital is a resource that parties represented in the parliament accumulate at the end of every day.

At the end of the day, each player/party in the Nomic Parliament gets *k* Political Capital, where *k* is equal to 50 divided by the square root of the party’s distance from the Indicator Puck (with any distance below 1 rounded up to 1), truncated to an integer. Distance is calculated after motion vote resolutions, but before elections.

At any time, players can redeem 100 Political Capital to receive 1 humor of their choosing. At any time, players can redeem 300 Political Capital to give another player 1 humor of the redeeming player’s choosing.

## 352

***Political Atmosphere***  
A 100 by 100 grid is created. A puck called the “*political atmosphere puck*” is created. The puck is one by one pixel. At time of passing, the puck is at (50, 50), which is the center of the grid.

The vertical axis is the “trust” axis and represents the level of trust that players have in the Nomic VII parliament.  
The horizontal axis is the “economy” axis and represents the state of the economy in Nomic VII.

When parties’ strategies are revealed, the *political atmosphere* puck is moved.

For each party who chooses a *selfish* strategy, the *political atmosphere puck* moves down by one pixel and left by one pixel.  
For each party who chooses a *selfless* strategy, the *political atmosphere puck* moves up by one pixel and right by one pixel.  
For each party who chooses a *nutritious* strategy, the *political atmosphere puck* moves down by one pixel and to the right by one pixel

When motions are either passed or not passed, the *political atmosphere puck* is moved.  
When a motion is passed, the *political atmosphere puck* moves up by one pixel and to the left by one pixel.  
When a motion is not passed, there is no change.

## 353

Proposal: Goose Master

The Golden Goose has an empty nest with room for two eggs. Eggs represent goose rules.

All players must always abide by all the goose rules then in effect, in the form that they are then in effect.

A goose rule’s gander is a number used for reference. The set of goose rules currently in effect shall never contain multiple goose rules with the same gander. A goose rule’s gander cannot be modified. The first goose rule receives the gander G-101. Each goose rule receives a successive gander.

A goose rule’s body is the main section of the goose rule. For any goose rule currently in effect, players must always abide by the contents of a goose rule's body.

Each week, the Golden Goose pecks two active players. Those players may submit a new goose rule each to become an egg in the nest. If both players submit a goose rule, any old goose rules are replaced. If neither player submits a goose rule, the old goose rules stand. If only one of the pecked players submits a goose rule and there are two existing Goose rules, they must choose which goose rule their new goose rule replaces. If they fail to specify a goose rule, the mods instead vote on which is to be replaced. This replacement occurs at the start of the turn following the turn that the goose rule(s) was submitted.

Goose rules aren’t rules. Goose rules cannot have retroactive application or change prior game state. Goose rules cannot make changes to the ruleset. Goose rules cannot supersede the normal ruleset. Goose rules cannot be amendments to rules or goose rules. Goose rules cannot change the properties of rules. Goose rules cannot reference any specific actions or mechanics in the normal ruleset other than implicitly by their association with this rule.

If a goose rule would impose consequences on a player for failing to perform an action it describes, they may instead do one of the following:  
1\) Post a recording of themselves loudly honking and gain 1 choleric humor  
2\) Lose 5 fame

## 354

Proposal: Goodnight Moon

At 10pm server time each day, Nomitron posts the text of a chapter from a public domain children's book, starting with the first chapter and continuing in order each day. Books are chosen in a vote by the moderators. When Nomitron posts the final chapter of a book, a new book must be voted on in the same manner.

After Nomitron has posted the chapter for the day, players can get comfy in bed. Once a player has declared that they are comfy in bed, they are considered comfy in bed until the end of the day. Declaring comfiness during a day before Nomitron has posted the daily chapter has no effect. Posting in any text channels while comfy in bed will cause the offending player to lose 1 sanguine humor and cease being comfy for that day. Players who stop being comfy via this method cannot become comfy again that day. If a comfy player last to the next day without posting in any text channels, they instead gain 1 phlegmatic humor.

## 362

***Fortune***

***Channels***  
A new group of channels called Fame and Fortune is created.  
In this group, three text channels are created. #The-Market-Spam, #The-Market, and  #The-Bank-Of-Nomitron

All commands for this rule should be done in #The-Market-Spam. Players are encouraged to mute this channel.

***Fortune***  
Fortune is created. Fortune is a currency that players can have and use in various ways. Fortune is in 0.01 increments and uses the ₦ symbol.  
Players may not purposefully/intentionally make their Fortune amount negative, but game mechanics can make their Fortune negative.  
Players who have more than ₦500 fortune are “Rich”. Players who have less than zero but greater than or equal to ₦-50.00 are “In-Debt”. Players who have less than ₦-50.00 are “Broke”.

***Roles***  
A new role is created called Loan Shark.  
Players may give themselves (and remove from themselves) the Loan Shark role in #Game-controls  
This role tells other players that they are ready/willing to offer loans.  
Nomitron also gains the Loan Shark role.

***The Market***  
*Goods*  
Players may buy and sell fruits in the market.  
Players may only list up to 3 fruits at a time. Players cannot list fruits that are on their team on the market. Fruits can only be listed as being between  ₦-5.00 and ₦10.00.  
Players can offer to buy up to 3 fruits at a time. They can only offer between ₦-5.00 and ₦10.00 per fruit.
<br>

*Services*  
Players may create job listings in the market.  
Players may use /ListJob (job description) (deadline) (payment amount). Players can pay either per job, per time, or something else relating to the job. Payments must be in the form of Fortune. Players cannot list jobs for less than or equal to ₦0.00.  
Players may only list up to three jobs at a time.  
Players cannot make listings for more fortune than they have.  
Once a job listing is accepted, it is removed from the job market.  
Players can only take one job at a time.  
Once they have completed a job, players may mark the job as completed to receive their payment.

Paragons may create up to five job listings in the market in the same way. The amount of money that is paid out from paragon job listings comes from a “community fund”.

Job listings and fruit listings should be posted by Nomitron in #The-Market.

***Bank of Nomitron (The BON)***  
The Bank of Nomitron is the Nomic VII bank.  
The BON is closed on Mondays, as Mondays are a bank holiday.  
Nomitron is the BON’s banker and loan officer.  
In #The-Bank-Of-Nomitron, player loan statuses are posted. These statuses are public. Also posted here is the current BON loan information (Payback and accumulation amounts).  
Each Monday at 9am, the BON deletes all its previous job listings and creates new ones. Once a job listing is accepted, it is removed from the job market. The job listings posted each monday by the BON are:  
Create a proposal that is at least 100 words. (₦2.50 per 50 words)  
Create a proposal that is at least 100 words. (₦2.50 per 50 words)  
Create a proposal that is at least 300 words. (₦4.50 per 50 words)  
Give feedback to another player about their proposal. (₦5 per job)  
Give feedback to another player about their proposal. (₦5 per job)  
Vote on proposals (₦10 per vote)  
Vote twice on proposals (₦8 per vote)  
Vote three times on proposals (₦6 per vote)  
Guess your fruit correctly once (₦10 per correct guess)  
Guess your fruit correctly twice (₦8 per correct guess)  
Guess your fruit correctly three times (₦6 per correct guess)  
Endorse one proposal (₦6 per endorsement)  
Endore two proposals (₦4 per endorsement)  
Win a fight in the arena (₦20 per win)  
Win two fights in the arena (₦15 per win)  
Successfully get another player to post a proposal (₦20 per job)  
Successfully get another player to post a proposal (₦20 per job)  
Engage in a fruit battle with another player (₦20 per job)  
Engage in a fruit battle with another player (₦20 per job)  
Get someone to join Nomic. This is considered complete once the new player engages with at least one game mechanic. (₦175 per job)

Players cannot accept a job if they already have one. Players have seven days after accepting to complete their BON issued job.

***Loans***  
*BON Loans*  
Players may take out loans from the BON and/or from other players.  
Players may make payments to their own loan. They may also make payments to other player’s loans.  
When a player takes out a loan, this means they have a “payback amount”. This is the amount of money they owe to a given loan shark.

Players may only take out one BON loan at a time. Players can only have one loan at a time from the BON.  
BON loans work as such:

At the time a loan is taken out, the payback amount is a percent of the loan amount.  
The percent of the loan amount is determined by the trust in parliament.  
If the trust in the parliament is  
greater than or equal to 90, then it is 100%.  
greater than or equal to 80 and less than 90 , then it is 105%.  
greater than or equal to 70 and less than 80 , then it is 110%.  
greater than or equal to 60 and less than 70 , then it is 115%.  
greater than 50 and less than 60 , then it is 120%.  
equal to 50, then it is 125%.  
greater than or equal to 40 and less than 50 , then it is 130%.  
greater than or equal to 30 and less than 40 , then it is 135%.  
greater than or equal to 20 and less than 30 , then it is 140%.  
greater than or equal to 10 and less than 20 , then it is 145%.  
greater than or equal to 0 and less than 10 , then it is 150%.

After five days, the BON payback amount begins to accumulate at a percent of the total amount each day at 5pm, not including Mondays  
The percent of accumulation is determined by the location of the indicator puck.  
greater than or equal to 90, then it is 1%.  
greater than or equal to 80 and less than 90 , then it is 1.5%.  
greater than or equal to 70 and less than 80 , then it is 2%.  
greater than or equal to 60 and less than 70 , then it is 2.5%.  
greater than 50 and less than 60 , then it is 3%.  
equal to 50, then it is 3.5%.  
greater than or equal to 40 and less than 50 , then it is 4%.  
greater than or equal to 30 and less than 40 , then it is 4.5%.  
greater than or equal to 20 and less than 30 , then it is 5%.  
greater than or equal to 10 and less than 20 , then it is 5.5%.  
greater than or equal to 0 and less than 10 , then it is 6%.

*Loan Shark Loans*  
Players must have the Loan Shark role to give loans to other players.  
Players may take out up to four loans from players. They can only take out one loan from each player at a time.  
Player loans work as such: At the time a loan is taken out, the payback amount is 100% of the loan amount. After seven days, the payback amount gains 5% of the original loan amount each day at 5pm, not including Mondays.

Players must pay a minimum of 25% of their original loans for each loan they have per turn. If they do not, the amount is removed from their fortune anyways. If this causes them to go into a negative amount, they will lose 10 fame for every ₦5 past ₦0.00 they go.

***Player Statuses***  
*“In Debt”*  
If a player is “In Debt”, they cannot buy items that would lessen their amount of fortune. Players who are in debt cannot loan money nor can they be a loan shark. Players who are in debt cannot create any job listings.

*“Broke”*  
If a player is “Broke”, they cannot buy items that would lessen their amount of fortune. Players who are broke cannot loan money nor can they be a loan shark. Players who are broke cannot create any job listings. Players who are broke must accept one job listing per turn that they are broke during. If there are no jobs available, they will be given a Nomic job.

*“Rich”*  
If a player is “Rich”, they can buy items. Players who are rich cannot take out loans. Players who are rich can loan up to 90% of their fortune.  
Rich players may donate up to ₦5.00 to broke players. They may only donate to broke players up to 3 times per turn.  
Rich players may “donate” to a paragon candidate's campaigns while a vote for paragon is active. For every ₦1000 donated during a given paragon candidate's campaign, one vote is added to the candidate's vote total, ₦750 goes into the “community funds”, and ₦250 is given to the candidate the extra vote is casted for.  
Rich players can donate ₦250, ₦500, or ₦750 to community funds up to 7 times per turn. A player gains 5 fame per hundred ₦ they donate.

*Other*  
Players who are not in debt, broke, or rich can participate in the market, take and give loans, and take and post job listings. They cannot tip or donate to community funds.  
Players who are not in debt, broke, or rich can remove 3 rot from themselves per turn.

## 367

Alarm:  
players may set an alarm through nomitron. This alarm will private message them when their alarm is set to ring. Players may request the alarm to alert them to: a specific date, time, game event. the command /alarmset followed by one of the above modes can be done in #actions

## 368

Proposal: Totino’s For My Hungry Guys

An in-game entity is an ingredient if the following is true:

1\) There can only be exactly one owner of any one single quantity of said entity.  
2\) There is nothing distinguishing any one single quantity of said entity with any other single quantity of said entity aside from ownership.  
3\) It is not explicitly allowed to have a non-integer or negative quantity of said entity.  

If an action would lead to a player having a negative or non-integer quantity of an item, that action is blocked.

## 370

***The other six: Pride, Envy, Wrath, Sloth, Greed, and Lust.***

**Pride**  
Players with an amount of fame less than negative 100 or greater than positive 100 and/or players with more than 10 of any fame may, once per turn, *boast*.  
Players who boast gain 5% of their fame and gain 1 of any humor of their choice.  
Players who boast cannot also *pout*, become *vengeful*, *chill*, or *hoard* at the same time.

**Envy**  
Players who have more than -100 fame but less than +100 fame may, once per turn, *pout*.  
Players who pout may choose to steal either 3% of another player's fame or 3% of their fortune. Players who pout can only steal from one other player per turn.  
Players who have been stolen from during a turn cannot be stolen from again.  
Players who pout cannot also *boast*, become *vengeful*, *chill*, or *hoard*at the same time.

**Wrath**  
Players who have been stolen from may, once per turn, become *vengeful*.  
Players who are vengeful may do one of four things to the player that stole from them.
1. They may incur 3 rot
2. They may incur -10 fame
3. They may incur -1 humor of their choice
4. They may incur -10 fortune

Players who become *vengeful* cannot also *boast*, *pout*, *chill* or *hoard* at the same time.

**Sloth**  
Players may choose, once per turn, to begin to *chill*.  
When *chilling*, a player loses 1% of their fame, 1% of their fortune, and 1 of their humor of choice every other day.  
Players who *chill* cannot also *boast*, become *vengeful*, *hoard*or *pout* at the same time.  
If a player who is *chilling* is stolen from, they stop *chilling*.  
Players may stop *chilling* at any time.

**Greed**  
Players may choose, once per turn, to begin to *hoard*.  
When *hoarding*, a player accumulates 1% of their fortune each day and gains 1 of their humor of choice every other day.  
If a player who is hoarding is stolen from, they stop hoarding and become *vengeful*.  
Players who *hoard* cannot also *boast*, become *vengeful*, *chill* or *pout* at the same time.  
Players can stop *hoarding* after eight days.

**Lust**  
Players may, once per turn, invite another player to *commune* with them. If the other player consents, the players may pick one of their humors each. The humor amount of each is added to the other, and then this number is divided by two. The nearest rounded down whole number for each type becomes the new amount for each respective humor.  
Players who *commune* cannot become *vengeful* against each other in the same turn.

## 371

Every Monday, players may submit one Outfit of the Week or "OOTW," which is to be judged in fashion show style. Participation is voluntary and open to players who are active at the beginning of the week. Submissions may be real-life photos, drawings/renderings, or video game screenshots, but they must be original content demonstrated on a humanoid model.

A dice roll selects a category from this list: [https://perchance.org/5c0xph1uer]. If the selected category is the same as the previous week's, the dice will be rolled until a different one is selected. If no one likes the category selected, a new one may be chosen by a majority vote from moderators.

Submissions are closed four days/96 hours after the start of the week, at which time players judge the outfits. Each outfit can be voted on once per person on a scale of 1 (worst) to 5 (best), based on the adherence to the category. Players have until the end of the week to vote, and results are released the following day at 1200 NST (Nomic Standard Time). The total vote scores are averaged per player, per outfit.

Players can only make one submission but can edit or amend their outfit 3 times before the judgment period begins. Once the judgment period begins, no more submissions or edits may be made. A player can withdraw their submission but cannot submit another that week.

The first, second, and third-place winners get their outfits printed in The Nom Times if they wish. First-place winners receive 5 fame and 1 humor of their choice, Second-place winners receive 3 fame and 1 random humor, and Third-place winners receive 1 fame. In the event of a tie, players split the rewards, rounding up if the result would be a non-whole integer.

## 373

All players have a positive or negative integer amount of fame. Anytime a rule attempts to set a player’s fame it is rounded up. A negative fame represents infamy rather than a lack of notoriety.  
Players with less than -100 fame are nefarious.  
Players with less than or equal to -25 infamous.  
Players with more than or equal to 25 fame are famous.  
Players with more than 100 fame are influential.

Fame cannot be the sole qualification of a win condition.

All new players start with zero fame.  
Other rules may add or subtract fame.  
Fame can also be tied to a specific status such that whenever you gain that status you gain fame, and loose fame when you loose that status.

Having a new rule you proposed enacted as a rule gives you +4 fame per rule. Amendments and repeals do not grant this bonus.  
Having an amendment you proposed enacted gives you +2 fame per amendment.  
Having a repealment you proposed enacted gives you -3 fame per repealment.

## 378

The Magic System Rule

Players who declare themselves Spellcasters (different from the wizard class tied to the arena) are allowed to cast or craft spells.

Being a Spellcaster reduces any fame gained or lost by 25%

If a spellcaster has imbalanced humors they cannot cast more than one spell per turn and they may not craft any spells.

Otherwise only 3 spells can be cast per turn, and one spell may be crafted per turn.

Any Spell that is cast has its effect end when the turn it was cast on ends, and no spell can last forever.

Castable Spells:

UGLY CURSE: this spell can be cast once per turn, and targets one player of the spellcasters choosing. The targeted player only gains 50% positive fame but gets 50% more negative fame for the rest of the turn.

VILE AURA: a spellcaster may target themselves and repel the effects of one mutable rule, (not including rules from the initial set.) This spell is only castable once per week.

DIARRHEA CURSE: any one player may be targeted. The targeted player poops so hard they lose one fruit.

Crafting Spells:

A minimum of three spellcasters are needed to craft a spell. A spell that is crafted must have a name, a specified target and must be vetted by a moderator before the spell is allowed to be castable.

## 379

hell yeah

## 381

**Article I, Section 3, Clause 4**

If there is a tie vote on a rule-change proposal by the end of its vote, and the vote for said proposal has not been extended, then the vote for said proposal is extended to the end of the following turn, superseding Rule 202. During this time, only the sitting Vice President of the United States may vote on the proposal. The vote may be provided directly in the server or through communications from the Office of the Vice President, either publicly or directly to a moderator.

If the moderators determine that the Vice President's vote is ambiguous, it will not be counted, but votes are not restricted to those set out by Rule 207, superseding Rule 207.

## 382

This rule has been left blank as an exercize for the player.

## 383

Rules are mining, crafting, and/or toggling the trap door.  
Amendments are always crafting.  
Repealments are always mining.

Rules that create a new action are mining.  
Rules that do something independent of other rules that are not an action are crafting.  
Rules that interact with other rules are toggling the trap door.

## 391

From when this clause comes into effect through December 22nd, players will create a list of awards that will be won at The Nomic Awards Ceremony. Award submissions under previous iterations of this rule are still considered valid.

At the start of December 23rd, award submissions are numbered in order of their submission and posted publicly. Over a period of 24 hours, players vote on whether or not to make the award submissions awards. If, following the end of the votes, an award submission has a simple majority of votes in its, favor, it will be considered an official reward.

At the start of December 24th, players may anonymously nominate other players for the set of official awards for a period of 96 hours (through December 27th). A player must receive at least two nominations for an award to be considered a contender for it.

At the start of December 28th, players may anonymously vote between the contenders of each award for the winner of each award for a period of 96 hours (through December 31st).

Award winners (except for those defined below) are resolved as follows:
<ol type="1">
  <li>All awards with no tie vote are awarded to their votes' winners</li>
  <li>For each award with a tie vote, in order of their submission's submission:
    <ol type="a">
      <li>If one of the winners has thus far won fewer awards than any of the other winners, they win the award</li>
      <li>If one of the winners has more seats in the Nomic Parliament than any of the other winners, they win the award</li>
      <li>Otherwise, the first winner to be nominated for the award wins the award</li>
    </ol>
  </li>
</ol>

When this clause comes into effect, a poll will be held among the playerbase to determine the time of the Nomic Awards Ceremony. The Nomic Awards Ceremony must occur during the first seven days of 2025, and its date and time must be announced based on the poll results no later than December 31st.

The Nomic Awards Ceremony will feature a Kahoot! game, to be set up by moderators and played by any players that did not set up the Kahoot!, about Nomic. Players that help create the Kahoot! all receive the Kahoot Creator Award, while the player with the highest score receives the Kahoot Conquerer Award.

Following the Kahoot!, all player-submitted awards will be presented along with their winners in order of their submission.

Following the last award's presentation, all players who have won at least one award win the game.

## 394

Horse armor is purchasable from Equardo for 10 fruit. When buying 10 random fruit are removed from your inventory. The armor can be used on any fruit in a party and adds an extra 1d3 attack roll.

## 398

Inactive, "On Leave", and dead players can still participate in the Kahoot! that is part of the Nomic Awards Ceremony. This rule supercedes Rules 340 and 344.

## 399

Inactive and Dead players can be nominated for and win awards.

If a player attempted to nominate an inactive and/or Dead player prior to the passing of this rule, that nomination is considered to be valid starting from the moment this rule takes effect.

Award nominations will re-open December 30th and 31st, during which award voting will be unavailable. Nominations that were attempted to be made on December 28th and December 29th are counted as nominations at the moment this rule takes effect. Players are considered nominated for an award as long as the receive at least one nomination. Award voting will re-open from the start of January 1st to 6:00 PM CST on January 3rd. Votes from the previous award voting period will still be considered valid.

This rule supersedes rules 340, 344, and 391.

## G-102 &nbsp;~"Goose Rule"

No one can use, nor react to messages with, the 🦆 emoji so long as this rule is active. Messages and reacts outside of the server are permitted.

## G-103 &nbsp;~"Goose Rule"

every player who has farted this week must apologize at least once to anyone in the room when it happened.
